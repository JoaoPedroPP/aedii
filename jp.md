# Atividade de AEDII
### Aluno: João Pedro Poloni Ponce
### RA:11116513

1. Em que consiste o método de Hashing? E em que consiste o denominado Hashing Perfeito?

O método de hashing consiste em aplicar uma função sobre um dado de modo a indexá-lo numa tabela, onde o resultado da operação é a chave, ou posição em que o dado foi armazenado. O hashing perfeito é quando a complexidade de tempo da busca é O(1) para o pior caso, ou seja, não há colisão.

2. Qual o melhor método de implementação de uma função de dispersão: Divisão, Multiplicação ou Dobra? Por quê?

A escolha da função de dispersão a ser utilizada depende de inumeros fatores como por exemplo complexidade da função e os recursos técnicos disponíveis. Dentre as três opções apresentadas e com foco em velocidade, o método da dobra na sua versão binária é melhor pois trata o dado diretamente em binário,simplificnado o trabalho da máquina, e consiste basicamente na função de XOR, as outras fazem uso do resto de uma divisão, uma operação mais custosa que XOR.

3. Como funciona o método de implementação de função de dispersão denominado de Análise de Dígitos? Qual dentre os seguintes métodos você escolheria: Divisão, Multiplicação, Dobra e Análise de Dígitos? Por quê?

O método da análise de digitos consiste em veriricar a distribuição dos digitos de um determinado dado e eliminar o mais frequente, este preocesso se repete até a função chegar a valor compatível a um endereço na tabela de dispersão. É um método muito útil para se trabalhar com arquivos estáticos e dados que são formados por uma regra base, tornado a fácil a analise. Com foco em tempo de execução, ou seja, velocidade, o método de dobra continua mais rápido justamente por trabalhar com os dados diretemente em binário e fazer operações simples.

4. Qual o melhor método de tratamento de colisões para uma tabela hash: Encadeamento Exterior, Encadeamento Interior ou Endereçamento Aberto? Por quê?

O encademento exterior trata as colisões de modo a formar uma lista encadeada na posição gerada pela função hash, configura-se uma desvantagem o fato de ela usar alocação alocação dinâmica pois os recursos são finitos, o encadeamento interior é dividio em zonas limitando a repetição de chaves, algo desvantajoso, o endereçamento aberto busca o melhor do dois, pois tem um tamanho fixo de chaves e permite mais colisões que o encademamento interior.

5. Considerando as perguntas acima e as questões da lista anexa, qual foi o grau de dificuldade desta prática (fácil, médio, difícil)? Por quê?

Médio. As questões exigiram revião total dos slides da aula bem como a busca por outros materiais de apoio para fornecer base suficiente a resposta, contudo as questões eram em suma reflexivas, provocando debate de ideias para encontrar reposta adequada.