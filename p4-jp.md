# Atividade de AEDII
### Aluno: João Pedro Poloni Ponce
### RA:11116513

1. Caracterize o processamento sequencial e o processamento não sequencial de arquivos. Em que cenário um ou outro poderia ser mais indicado?

<p style="text-align:justify">O processametno sequencial de arquivos pode ser caracterizado pelo forma como a máquina registra o dado a ser salvo, e ela faz isso pode meio de uma chave primária associada ao dado, de modo que quando for necessário utilizar os dados armazenados ele serão montados a partir da sequência de indices conformo o arquivo foi montado, devido natureza sequêncial da técnica, o processamento sequeêncial de arquivos funciona muito bem com o registro de dados em fitas magnéticas. As formas não sequencias de se utilizar arquivos são os chamados arquivos indexados, eles se fazem necessários quando a frequência de acesso aleátorio aumenta ao ponto de tornar o acesso sequencial e sequencial indexado ineficiente, outra forma de processamento de arquivos de forma não sequêncial é o acesso direto, onde o endereço do dado é determinando por uma função de hash. As duas formas de processamento não sequencial podem ser encontradas no HD do PC devida a natureza de acesso e tamanho dos arquivos salvos.</p>

2. Explique o conceito de gerência de memória virtual, incluindo o significado dos termos PAGE FAULT e PAGE HIT. Como esses conceitos são usados na avaliação do desempenho de um sistema computacional?

<p style="text-align:justify">O uso de memória virtual no gerenciamento de memória bastante utilizado e implementado pela grande maioria dos sistemas operacionais, o conceito consiste em o sistema operacional alocar uma estrutura semelhante a um array e adotar uma tabela de mapeamento, convertendo as posições do array em endereços físicos alocados na memória do PC podendo essa ser a principal assosciada a secundária de modo a aumentar a capacidade de armazenamento. O mapeamento pode ser feito de três formas: paginação, segmentação e segmentação com paginação, um modelo hibrido das formas anteriores; o processo de paginação consiste em alocar um bloco de mesmo tamanho, por ter um tamanho fixo, caso o arquivo armazenado seja grande pode haver caso em que paginação não mapeie tudo no mesmo bloco, gerando o PAGE FAULT, que significa que o dado não foi mapeado na memória principal e sim na secundária e precisa ser trazido de lá.</p>

3. Explore brevemente o conteúdo do artigo do sítio indicado abaixo (caso necessário, cadastre-se na base Researchgate, usando seu email da UFABC). Sobre o que ele trata? Como você relaciona com o conteúdo desta prática?

<p style="text-align:justify">O artigo trata sobre o desenvolvimento de sistemas de informação, focando no trabalho de conclusão de curso das intituições de ensino pesquisadas, a prtir das tecnicas de engenharia de software existentes como XP, scurm e RUP e também discute os pontos fortes e fracos de cada uma delas, ao final surge com uma proposta de customização do RUP voltado ao desenvolvimento do TCC dos alunos nas intituições pesquisadas. O artigo está relecionado com a máteria pelo fato de que os sistemas estudados em aula, como hash e manipulação de arquivos, constituem um software que pode ser feito uso de metoldologias como as apresentadas no artigo para serem desenvolvidas, o artigo final da disciplina poderá ser feito utilizando as metodologias discutidas no artigo.</p>

4. Considerando as perguntas acima e as questões propostas na lista anexa, como você avalia o grau de dificuldade desta prática (fácil, médio ou difícil)? Justifique sua resposta.

<p style="text-align:justify">Médio. As questões exigiram revisões que tomaram um certo tempo, tanto para refletir o funcionamento dos processo e seu desempenho no hardware selecionado.</p>



