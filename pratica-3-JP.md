# Atividade de AEDII
### Aluno: João Pedro Poloni Ponce
### RA:11116513

1. Compare, citando vantagens e desvantagens, os tipos de arquivo: sequencial e sequencial indexado.

Das formas de armazenamento apresentadas cada uma possui suas particularidades, tipo sequêncial, o mais intuitivo, busca organizar os dados como o proprio nome sugere em sequencia e de forma ordenada através de uma chave, é a mais simples forma de se armazenar, e funciona bem para arquivos pequenos, para arquivos maiores a sequecial indexada é mais vantajosa pis faz uso de estruturas de acesso, que buscam e organizam os dados do arquivo consultando um indice, logo ela indexa as chaves de acesso e encontra o dado buscando no indice da tabela, devido ao uso da estrura ele se comporta melhor para grandes arquivos, contudo não funciona bem em ambientes dinâmicos pois o indice acaba por sendo refeito com muita frequancia, uma operação custosa para a máquina.

2. Compare, citando vantagens e desvantagens, os tipos de arquivo: indexado e acesso direto.

Dos tipos citados de aquivos a que possui o acesso mais rápido é o acesso direto pois trata-se um método que consiste em aplicar uma função sobre uma chave de busca que retorna o endereço da dado a ser buscado, dado isso ele prove uma forma eficiente a de acesso a memória pois não há necessidade de estrutura auxiliar para buscar dados, diferentemente do indexado que, organiza o arquivo a partir da consulta a um indice, o arquivo inteiro é montado com base nele.

3. Explore brevemente o conteúdo do artigo do sítio indicado abaixo (caso necessário, cadastre-se na base Researchgate, usando seu email da UFABC). Sobre o que ele trata? Como você relaciona com o conteúdo desta prática?

O artigo discute as abordagens existentes sobre a forma de modelar um banco de dados, inicialmente comparando o modelos relacional e orientado a objeto para depois analisar mais críticamente e de modo competitivo, buscando extrair a melhor forma de modelagem. A melhor técnica foi obtida por meio de entrevistas a profissionais da área, que concluíram que o modelo relacional é o prefereido do mercado apesar do modelo orientado a objetos possuir um futuro promissor. 

4. Considerando as perguntas acima e as questões propostas na lista anexa, como você avalia o grau de dificuldade desta prática (fácil, médio ou difícil)? Justifique sua resposta.

Médio. Acabei por faltar na aula teórica, o que me obrigou enteder por conta todo o material antes de começar a responder as perguntas citadas. Conteúdo denso e cheio de particularidades.