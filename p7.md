# Atividade de AEDII
### Aluno: João Pedro Poloni Ponce
### RA:11116513

1. Defina a  árvore de Huffman, mencionando as suas principais características.

<p style='text-align:justify'>Trata-se de uma árvore binaria onde o filho esquerdo representa o zero e filho direto o um e o nó o dado. Ela busca comprimir os dados de forma que o caminho até determinado nó seja o dado comprimido. A árvore de huffman faz com que o dado mais frequente dentro do conjunto a ser comprimido possua a menor codificação para compressão, realizando assim uma boa compressão, ela também possui algumas particularidades como por exemplo os nós não podem ser prefixos pois assim a sequência será perdida e não será possível descompactar os dados codificados.</p>

2. Considerando o projeto do algoritmo de compressão de Huffman, destaca-se o possível emprego da estrutura de dados heap. Explique como essa estrutura de dados é empregada e proponha uma outra estrutura de dados alternativa. A alternativa proposta por você é tão ou mais eficiente que o heap? Justifique  a sua resposta.

<p style='text-align:justify'>A heap é formada e utilizada como dicionario para identificar os códigos que representam os dados a serem comprimidos, ao utilizar tal estrutura é possível fazer uso de suas propriedades, como por exemplo busca em O(logn). É possivel utilizar a estrura de lista para compor os códigos de Huffman, onde cada elemento da lista seria composto por um codigo e o dado orignal, contudo esta estrura não é tão eficiente quanto comparado a heap pois sua busca é feita em O(n), ou seja, no pior caso gasta mais tempo que a heap</p>

3. Compare os seguintes tipos de algoritmos de compressão: Huffman e Run-Length Encoding. Em sua resposta, identifique vantagens e desvantagens comparativas.

<p style='text-align:justify'>O método Run-Length consite basicamente em comprimir os dados informando quantas vezes determinada dado se repete na sequência de forma continua, ou seja, a sequência a ser comprimida é composta por muitos dados iguais em sequência. A codificação Run-Lenght é ideal para situações onde os dados serem comprimidos é pouco entrópico, de modo que a probabilidade de ocorrência de determinado dado é maior que a dos outros, desta forma informar a quantidade de vezes que ele repete se mostra muito eficiente, por outro lado para dados muito entropicos a codificação de huffman se mostra mais adequada pois ela faz uso da própria entropia dos dados como vantagem.</p>

4. Pesquise sobre o método de compressão Ziv-Lempel e explique (brevemente) em que consiste a sua ideia de compressão.

<p style='text-align:justify'>A codificação Zip-Lampel, ou LZW, tem como ideia central a analise do dado a ser comprimido a construção de um dicionário que seja capaz de construir todos os dados a serem compactados, os dados comprimidos devem ser constituidos pelos códigos do dicionário e quantas vezes ele deve ser reptido, aproveitandoa ideia do Run-Lenght. Quando mais enxuto for este diconário melhor será a compactção dos dados.</p>

5. Considerando as perguntas acima e as questões da lista anexa, como você avalia o grau de dificuldade desta prática (fácil, médio ou difícil)? Justifique sua resposta.

<p style='text-align:justify'>Difícil. Exigiu revisão da aula e busca por novos materiais, além do fato de se tratar de um assunto complexo.</p>